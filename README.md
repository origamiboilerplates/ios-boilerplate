Boilerplate Skeleton Readme

Requirements
	•	Xcode 8+

Installation
	•	Just add the “Boilerplate” folder in your project.
	•	Install following pods
	⁃	  pod 'ObjectMapper', '~> 2.2'
	⁃	  pod 'Alamofire'
	⁃	  pod 'SwiftyJSON'
	⁃	  pod 'AASquaresLoading', '~> 1.0'
	⁃	  pod 'EZSwiftExtensions'
	⁃	  pod 'SDWebImage'

Usage
1. Add all of your controllers in “ViewControllers” folder.
2. Add all of your business models in “Models” folder.
3. Explore “Helpers” folder and place all the files in its named folder.
4. Extend all of your models “Mappable” and import ObjectMapper
### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact