
//
//  APIContants.swift
//  BandPass
//
//  Created by Arslan Ilyas on 12/01/16.
//  Copyright © 2016 Rapidzz. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

extension API{
    
    func handleResponse(parameters : JSON?) -> AnyObject? {
        switch self {
           
        case .getQuotations(_):
            
            let quoteArray = Mapper<Quote>().mapArray(JSONArray:parameters?.dictionary?["data"]?.rawValue as! [[String : Any]])
            return quoteArray as AnyObject?
            
        case .getAllMatchRecords():
            
            let matchesArray = Mapper<MatchResult>().mapArray(JSONArray:parameters?.dictionary?["data"]?.rawValue as! [[String : Any]])
            return matchesArray as AnyObject?
            
        case .getBreakingNews():
            
            let newsArray = Mapper<News>().mapArray(JSONArray:parameters?.dictionary?["data"]?.rawValue as! [[String : Any]])

            return newsArray as AnyObject?
        case .getAllNewsRecords():
            
            let newsArray = Mapper<News>().mapArray(JSONArray:parameters?.dictionary?["data"]?.rawValue as! [[String : Any]])
            return newsArray as AnyObject?
        
        case .getAllLeagusRecords():
            
            let leaguesArray = Mapper<League>().mapArray(JSONArray:parameters?.dictionary?["data"]?.rawValue as! [[String : Any]])
            return leaguesArray as AnyObject?
        }
    }
}
enum APIValidation : String{
    case None
    case Success = "1"
    case ServerIssue = "500"
    case Failed = "0"
    case TokenInvalid = "401"
}


enum APIResponse {
    case Success(AnyObject?)
    case Failure(String?)
}
