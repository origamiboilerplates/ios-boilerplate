
//
//  APIContants.swift
//  BandPass
//
//  Created by Arslan Ilyas on 12/01/16.
//  Copyright © 2016 Rapidzz. All rights reserved.
//

import Foundation


internal struct APIConstants {
    
    static let BasePath = "http://eegamesstudio.com/dev/Kfshkendija/index.php/KfshkendijaAPI/"
//    static let ImagePath = "http://eegamesstudio.com/dev/Kfshkendija/index.php/uploads/"
}


internal struct APIPaths {
  
    static let getQuotationsAgainstCategory = "getQuotationsAgainstCategory"
    static let getAllMatchRecords = "getAllMatchRecords"
    static let getBreakingNews =  "getBreakingNews"
    static let getAllNewsRecords = "getAllNewsRecords"
    static let getAllLeagusRecords = "getAllLeagusRecords"
    
}


internal struct FormatParameterKeys{
    static let method = "method"
    static let categoryID = "categoryID"
    static let leagueID = "leagueID"
   
}

internal struct APIParameterConstants {

    struct Quotes
    {
        let full = APIConstants.BasePath+APIPaths.getAllNewsRecords
        static let Quotes = [FormatParameterKeys.categoryID]
    }
    struct Categories {
        static let Categories = [FormatParameterKeys.method]
    }
   
}
