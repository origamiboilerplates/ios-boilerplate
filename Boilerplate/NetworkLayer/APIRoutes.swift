
//
//  APIContants.swift
//  BandPass
//
//  Created by Arslan Ilyas on 12/01/16.
//  Copyright © 2016 Rapidzz. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias OptionalDictionary = [String : String]?
typealias OptionalSwiftJSONParameters = [String : JSON]?

infix operator =>
infix operator =|
infix operator =<
infix operator =/

func =>(key : String, json : OptionalSwiftJSONParameters) -> String?{
    return json?[key]?.stringValue
}

func =<(key : String, json : OptionalSwiftJSONParameters) -> Double?{
    return json?[key]?.double
}

func =|(key : String, json : OptionalSwiftJSONParameters) -> [JSON]?{
    return json?[key]?.arrayValue
}

func =/(key : String, json : OptionalSwiftJSONParameters) -> Int?{
    return json?[key]?.intValue
}

prefix operator ¿
prefix func ¿(value : String?) -> String {
    return value.unwrap()
}


protocol Router {
    var route : String { get }
    var baseURL : String { get }
    var parameters : OptionalDictionary { get }
    var method : Alamofire.HTTPMethod { get }
}



enum API {
    
    static func mapKeysAndValues(keys : [String]?,values : [String]?) -> [String : String]?{
        guard let tempValues = values,let tempKeys = keys else { return nil}
        var params = [String : String]()
        for (key,value) in zip(tempKeys,tempValues) {
            params[key] = ¿value
        }
        return params
    }
    
    case getQuotations(categoryID:String?)
    case getAllMatchRecords()
    case getBreakingNews()
    case getAllNewsRecords()
    case getAllLeagusRecords()
    
}


extension API : Router{
    
    var route : String {
        switch self {
         case .getQuotations(_):
            return APIPaths.getQuotationsAgainstCategory
        case .getAllMatchRecords():
            return APIPaths.getAllMatchRecords
        case .getBreakingNews():
            return APIPaths.getBreakingNews
        case .getAllNewsRecords():
            return APIPaths.getAllNewsRecords
        case .getAllLeagusRecords():
            return APIPaths.getAllLeagusRecords
        }
        
        
    }
    
    
    var baseURL : String {  return APIConstants.BasePath }
    
    var parameters : OptionalDictionary {
        return formatParameters()
    }
    
    func url() -> String {return baseURL + route}
    var method: Alamofire.HTTPMethod {
        
        switch self {
            
            
        case .getQuotations(_):
            return .get
        case .getAllMatchRecords():
            return .get
        case .getBreakingNews():
            return .get
        case .getAllNewsRecords():
            return .get
        case .getAllLeagusRecords():
            return .get
            
        }
    }
}

extension API {
    func formatParameters() -> OptionalDictionary {
        
        switch self {
            
         case .getQuotations(let categoryID):
            return API.mapKeysAndValues(keys: APIParameterConstants.Quotes.Quotes, values:[¿categoryID])
            
        case .getAllMatchRecords():
            return nil
        case .getBreakingNews():
            return nil
        case .getAllNewsRecords():
            return nil
        case .getAllLeagusRecords():
            return nil
        }
    }
}

