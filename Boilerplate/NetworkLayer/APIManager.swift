
//
//  APIContants.swift
//  BandPass
//
//  Created by Arslan Ilyas on 12/01/16.
//  Copyright © 2016 Rapidzz. All rights reserved.
//
import Foundation
import SwiftyJSON
import Alamofire

typealias APICompletion = (APIResponse) -> ()




class APIManager: NSObject {

    static let sharedInstance = APIManager()
    private lazy var httpClient : HTTPClient = HTTPClient()
    /// USe this method if you have configured all the routes in the app and just provide an object of API type as a parameter to this method
func opertationWithRequest ( withApi api : API , completion : @escaping APICompletion )  {
    httpClient.postRequest(withApi: api, success: { (data) in
        guard let response = data else {
            completion(APIResponse.Failure(""))
            return
        }
        let json = JSON(response)
        
        print("Json,API : \(json),\(api.url())")
        
        if let status = json.dictionaryValue["status"]?.int {
            if status == 400 {
                if let message = json.dictionaryValue["message"]?.stringValue {
                    completion(APIResponse.Failure(message))
                    return
                }
                completion(APIResponse.Failure(""))
                return
            }
        }
        
        completion(.Success(api.handleResponse(parameters: json)))
        
        }) { (error) in
        print(error)
    }
    }
/// This method can be used for any post request. provide full API url and parameters in a Dictionary
    func genericOperationWithRequest(apiURL: String, params: Parameters, success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure) {
        Alamofire.request(apiURL, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(let data):
                success(data as AnyObject?)
            case .failure(let error):
                failure(error as NSError)
                
            }
        }
        
    }
}



